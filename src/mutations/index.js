import gql from 'graphql-tag';

export const AUTENTICAR_ADMIN = gql`
  mutation autenticaradmin($email: String!, $password: String!) {
    autenticaradmin(email: $email, password: $password) {
      success
      message
      data {
        token
        id
      }
    }
  }
`;

export const NUEVO_USUARIO = gql`
  mutation crearUsuario($input: UsuarioInput) {
    crearUsuario(input: $input) {
      success
      message
      data {
        id
        nombre
        apellidos
        email
      }
    }
  }
`

export const ELIMINAR_USUARIO = gql`
  mutation eliminarUsuario($id: ID!) {
    eliminarUsuario(id: $id) {
      success
      message
    }
  }
`;


export const ACTUALIZAR_ADMIN = gql`
  mutation actualizaradmin($input: ActualizaradminInput) {
    actualizaradmin(input: $input) {
      id
      email
      nombre
      apellidos
      ciudad
      telefono
      avatar
      notificacion
      cargo
      twitter
      linkedin
      descricion
      createBy
    }
  }
`;

export const ELIMINAR_ADMIN = gql`
  mutation eliminarAdmin($id: ID!) {
    eliminarAdmin(id: $id) {
      success
      message
    }
  }
`;

export const NUEVA_MASCOTA = gql`
  mutation crearMascota($input: Mascotainput) {
    crearMascota(input: $input) {
      success
      message
      data {
        name
        age
        avatar
        especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
      }
    }
  }
`;

export const ELIMINAR_MASCOTA = gql`
  mutation eliminarMascota($id: ID!) {
    eliminarMascota(id: $id) {
      success
      message
    }
  }
`;


export const NUEVO_USUARIO_DEPOSITO = gql`
  mutation crearDeposito($input: DepositoInput) {
    crearDeposito(input: $input) {
      success
      message
      data {
        id
        fecha
        estado
        total
      }
    }
  }
`;

export const CREAR_CUPON = gql`
  mutation crearCupon($input: CuponCreationInput) {
    crearCupon(input: $input) {
      id
      clave
      descuento
      tipo
    }
  }
`;


export const READ_NOTIFICATION = gql`
  mutation readNotification($notificationId: ID!) {
    readNotification(notificationId: $notificationId) {
      success
      message
    }
  }
`;

export const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      success
      message
    }
  }
`;

export const ELIMINAR_CUPON = gql`
  mutation eliminarCupon($id: ID!) {
    eliminarCupon(id: $id) {
      success
      message
    }
  }
`;



export const NUEVO_ADMIN = gql`
  mutation crearadmin($input: userAdmininput) {
    crearadmin(input: $input) {
      success
      message
      data {
        id
        nombre
        createBy
        apellidos
        email
      }
    }
  }
`

export const ACTUALIZAR_PROFESIONAL = gql`
  mutation actualizarProfesional($input: ActualizarProfesionalInput) {
    actualizarProfesional(input: $input) {
      id
      email
      UserID
      nombre
      apellidos
      ciudad
      telefono
      avatar
      profesion
      experiencia
      descricion
      Precio
      notificacion
      isAvailable
      isVerified
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

export const ELIMINAR_PROFESIONAL = gql`
  mutation eliminarProfesional($id: ID!) {
    eliminarProfesional(id: $id) {
      success
      message
    }
  }
`;

export const ELIMINAR_CONSULTA = gql`
  mutation eliminarconsulta($id: ID!) {
    eliminarconsulta(id: $id) {
      success
      message
    }
  }
`;

export const CREAR_MODIFICAR_CONSULTA = gql`
  mutation crearModificarConsulta($input: ConsultaCreationInput) {
    crearModificarConsulta(input: $input) {
      id
      cupon
      nota
      aceptaTerminos
      endDate
      time
      cantidad
      descuento {
        clave
        descuento
        tipo
      }
      usuario{
        id
      }
      profesional{
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        avatar
        profesion
        experiencia
        descricion
        Precio
        notificacion
        isProfesional
        isVerified
        isAvailable

      }
      estado
      status
      progreso
      created_at
      mascota{
        id
        name
        age
        usuario
        avatar
      }
    }
  }
`;

export const PROFESSIONAL_CONSULTA_PROCEED = gql`
  mutation consultaProceed($consultaID: ID!, $estado: String!, $progreso: String!, $status: String!, $nota: String, $coment: String, $rate: Int, $descripcionproblem: String) {
    consultaProceed(consultaID: $consultaID, estado: $estado, progreso: $progreso, status: $status, nota: $nota, coment: $coment, rate: $rate, descripcionproblem: $descripcionproblem) {
      success
      message
    }
  }
`;



export const NUEVO_DIAGNOSTICO = gql`
  mutation crearDiagnostico($input: Diagnosticoinput) {
    crearDiagnostico(input: $input) {
      success
      message
      data {
      profesional {
        nombre
        apellidos
        profesion
        avatar
      }
      diagnostico
      mascota
      }
    }
  }
`;

export const ELIMINAR_DEPOSITO = gql`
  mutation eliminarDeposito($id: ID!) {
    eliminarDeposito(id: $id) {
      success
      message
    }
  }
`;


export const CREAR_DEPOSTIO = gql`
  mutation crearDeposito($input: DepositoInput) {
    crearDeposito(input: $input) {
      success
      message
      data{
        id
        fecha
        estado
        total
        profesionalID
        created_at
      }
    }
  }
`;

export const NUEVO_PAGO = gql`
  mutation crearPago($input: PagoInput) {
    crearPago(input: $input) {
      success
      message
      data {
        id
        nombre
        iban
        profesionalID
      }
    }
  }
`;


export const ELIMINAR_PAGO = gql`
  mutation eliminarPago($id: ID!) {
    eliminarPago(id: $id) {
      success
      message
    }
  }
`;
