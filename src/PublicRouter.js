import React from "react";
import { Route, Redirect } from "react-router-dom";

const PublicRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      (localStorage.getItem('token')) ? (
        <Redirect
          to={{
            pathname: "/welcome",
            state: { from: props.location }
          }}
        />

      ) : (
          <Component {...props} refetch={rest.refetch} />
        )
    }
  />
);

export default PublicRoute;