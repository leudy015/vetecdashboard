import React, { useState } from "react";
import "./Profile.css";
import Header from "../header";
import {
  Select,
  Card,
  Col,
  Row,
  Calendar,
  Tag,
  Alert,
  Spin,
  Avatar,
} from "antd";
import esES from "antd/es/locale/es_ES";
import {
  EnvironmentOutlined,
  WhatsAppOutlined,
  MailOutlined,
  TwitterOutlined,
  LinkedinOutlined,
} from "@ant-design/icons";
import TextLoop from "react-text-loop";
import { GET_TEAM, GET_TEAM_SEARCH } from "../../../query/index";
import { Query } from "react-apollo";
import { IMAGES_PATH } from "../../../constant/consfig";

const { Option } = Select;

const ProfileTeam = () => {
  let id = localStorage.getItem("id");

  const [search, setSearch] = useState(id);
  const [todo, setTodo] = useState(null);

  function onChange(value) {
    console.log(`selected ${value}`);
    setSearch(value);
  }

  function onBlur() {
    console.log("blur");
  }

  function onFocus() {
    console.log("focus");
  }

  function onSearch(val) {
    console.log("search:", val);
  }

  function onPanelChange(value, mode) {
    console.log(value.format("DD-MM-YYYY"), mode);
  }

  return (
    <div>
      <Header
        title="Miembro del equipo"
        extra={
          <Query query={GET_TEAM_SEARCH}>
            {(response) => {
              if (response.loading) {
                return (
                  <div className="page-loader">
                    <Spin size="large"></Spin>
                  </div>
                );
              }
              if (response.error) {
                return console.log("Response Error-------", response.error);
              }
              if (response) {
                const datas =
                  response && response.data && response.data.getTeamSearch
                    ? response.data.getTeamSearch.data
                    : "";
                setTodo(datas.length);
                return (
                  <div>
                    <Select
                      showSearch
                      style={{ width: 500, marginBottom: 50 }}
                      placeholder="Buscar miembro del equipo"
                      optionFilterProp="children"
                      onChange={onChange}
                      onFocus={onFocus}
                      onBlur={onBlur}
                      onSearch={onSearch}
                      filterOption={(input, option) =>
                        option.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      {datas &&
                        datas.map((mas, index) => {
                          return (
                            <Option key={index} value={mas.id}>{`${
                              mas ? mas.nombre : ""
                            } ${mas ? mas.apellidos : ""}`}</Option>
                          );
                        })}
                    </Select>
                  </div>
                );
              }
            }}
          </Query>
        }
        tit="Compañeros totales"
        total={todo}
      />
      <Query query={GET_TEAM} variables={{ id: search }}>
        {(response) => {
          if (response.loading) {
            return (
              <div className="page-loader">
                <Spin size="large"></Spin>
              </div>
            );
          }
          if (response.error) {
            return console.log("Response Error-------", response.error);
          }
          if (response) {
            const datas =
              response && response.data && response.data.getTeam
                ? response.data.getTeam.data
                : "";
            return (
              <div>
                <div className="porfiles">
                  <Avatar src={IMAGES_PATH + datas.avatar} size={120} />
                  <div className="porfiles-des">
                    <p>Activo</p>
                    <h1>{`${datas.nombre} ${datas.apellidos}`}</h1>
                    <Tag color="geekblue" className="tag">
                      {datas.cargo}
                    </Tag>
                    <h6 className="description">{datas.descricion}</h6>
                  </div>
                </div>
                <div className="porfiles-cont">
                  <div className="porfiles-items">
                    <div className="cards">
                      <div className="site-card-wrapper">
                        <Row gutter={16}>
                          <Col span={8}>
                            <Card title="Contacto" bordered={false}>
                              <div
                                style={{ display: "flex", marginBottom: 15 }}
                              >
                                <WhatsAppOutlined
                                  style={{ fontSize: 28, color: "#87d068" }}
                                />{" "}
                                <h3 style={{ marginLeft: 10 }}>
                                  {datas.telefono}
                                </h3>
                              </div>
                              <div
                                style={{ display: "flex", marginBottom: 15 }}
                              >
                                <MailOutlined style={{ fontSize: 28 }} />{" "}
                                <a
                                  className="links"
                                  href={`mailto:${datas.email}`}
                                  target="blak"
                                  style={{ marginLeft: 10 }}
                                >
                                  Email
                                </a>
                              </div>
                              <div
                                style={{ display: "flex", marginBottom: 15 }}
                              >
                                <EnvironmentOutlined style={{ fontSize: 28 }} />{" "}
                                <h3 style={{ marginLeft: 10 }}>
                                  {datas.ciudad}
                                </h3>
                              </div>
                              <div
                                style={{ display: "flex", marginBottom: 15 }}
                              >
                                <TwitterOutlined style={{ fontSize: 28 }} />{" "}
                                <a
                                  className="links"
                                  href={datas.twitter}
                                  target="blak"
                                >
                                  Twitter
                                </a>
                              </div>
                              <div
                                style={{ display: "flex", marginBottom: 15 }}
                              >
                                <LinkedinOutlined style={{ fontSize: 28 }} />{" "}
                                <a
                                  className="links"
                                  href={datas.linkedin}
                                  target="blak"
                                >
                                  Linkedin
                                </a>
                              </div>
                            </Card>
                          </Col>
                          <Col span={8}>
                            <Card title="Actividades" bordered={false}>
                              <Alert
                                banner
                                message={
                                  <TextLoop mask>
                                    <div>Notice message one</div>
                                    <div>Notice message two</div>
                                    <div>Notice message three</div>
                                    <div>Notice message four</div>
                                  </TextLoop>
                                }
                              />
                            </Card>
                          </Col>
                          <Col span={8}>
                            <Card title="Agenda" bordered={false}>
                              <Calendar
                                onPanelChange={onPanelChange}
                                locale={esES}
                                fullscreen={false}
                              />
                            </Card>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          }
        }}
      </Query>
    </div>
  );
};
export default ProfileTeam;
