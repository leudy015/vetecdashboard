import React from 'react';
import {
  Form,
  Input,
  Checkbox,
  Button,
  message,
} from 'antd';
import { MailOutlined, LockOutlined, UserOutlined } from '@ant-design/icons';
import { NUEVO_ADMIN } from '../../mutations'
import { Mutation } from 'react-apollo';
import { useHistory } from "react-router-dom";


const NewAdmin = props => {

    const history = useHistory()
    const [form] = Form.useForm();

    let id = localStorage.getItem('id');

  return (
    <Mutation mutation={NUEVO_ADMIN}>
    {(crearadmin) => {
      return (
    <div className="sub-containers">
    <Form
      form={form}
      name="register"
      onFinish={ values => {
        const handleSubmit = async () => {
            await crearadmin({variables: {
                input: {
                  nombre: values.nombre,
                  apellidos: values.apellidos,
                  email: values.email,
                  password: values.password,
                  createBy: id
                }
              }
            }).then(async ({ data: res }) => {
              if (res && res.crearadmin && res.crearadmin.success) {
                message.success(res.crearadmin.message);
                window.location.reload();
              } else if (res && res.crearadmin && !res.crearadmin.success)
                message.error(res.crearadmin.message);
            });
          }
          handleSubmit()
      }}
      scrollToFirstError
    >

    <Form.Item
        name="nombre"
        rules={[
          {
            required: true,
            message: 'Por favor ingrese su nombre!',
          },
        ]}
      >
        <Input 
        prefix={<UserOutlined className="site-form-item-icon" />}
        placeholder="Nombre"
        />
      </Form.Item>

      <Form.Item
        name="apellidos"
        rules={[
          {
            required: true,
            message: 'Por favor ingrese su apellidos!',
          },
        ]}
      >
        <Input 
        prefix={<UserOutlined className="site-form-item-icon" />}
        placeholder="Apellidos"
        />
      </Form.Item>

      <Form.Item
        name="email"
        rules={[
          {
            type: 'email',
            message: 'Email incorrecto!',
          },
          {
            required: true,
            message: 'Por favor ingrese su Email!',
          },
        ]}
      >
        <Input 
        prefix={<MailOutlined className="site-form-item-icon" />}
        placeholder="Email"
        />
      </Form.Item>

      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: 'Por favor ingrese una contraseña!',
          },
        ]}
        hasFeedback
      >
        <Input.Password 
        prefix={<LockOutlined className="site-form-item-icon" />}
        placeholder="Contraseña"
        />
      </Form.Item>

      <Form.Item
        name="confirm"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Por favor consfirme su contraseña!',
          },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }

              return Promise.reject('Las dos contraseñas que ingresaste no coinciden!');
            },
          }),
        ]}
      >
        <Input.Password 
        prefix={<LockOutlined className="site-form-item-icon" />}
        placeholder="Contraseña"
        />
      </Form.Item>
      <Form.Item>
        <Button shape="round" type="primary" htmlType="submit">
          Añadir miembro al equipo
        </Button>
      </Form.Item>
    </Form>
    </div>
    );
      }}
    </Mutation>
  );
};

export default NewAdmin;