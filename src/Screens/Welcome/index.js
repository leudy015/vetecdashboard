import React, {useState} from 'react'
import './index.css'
import { useHistory } from "react-router-dom";
import { Layout, Menu, PageHeader, Tag, Badge, Typography, Spin, Tooltip, Modal, Empty, Popover} from 'antd';
import {
  MenuUnfoldOutlined,
  DashboardOutlined,
  UserSwitchOutlined,
  UserOutlined,
  LogoutOutlined,
  BellOutlined,
  QuestionCircleOutlined,
  EditOutlined,
  PlusCircleOutlined,
  FileDoneOutlined,
  TeamOutlined,
  QqOutlined,
  EuroOutlined,
  ExclamationCircleOutlined,
  PercentageOutlined
} from '@ant-design/icons';
import Logo from '../../assets/image/icon.png'
import {DropdownMenu} from '../Componentes/Headers';
import {Query} from 'react-apollo'
import { IMAGES_PATH } from '../../constant/consfig';
import { ADMIN_DETAIL } from '../../query';
import MiCuenta from './Micuenta';
import NewAdmin from './NewAdmin';
import Dashboard from './dashboard';
import Profesionales from './profesionales/index';
import MiConsulta from './Consultas/Miconsultas';
import Clientes from './clientes';
import ProfileTeam from './ProfilesTeam';
import Mascotas from './mascotas/index';
import Deposito from './Depositos';
import Cupon from './TodoCupones';
const { Paragraph } = Typography;

const {Sider, Content } = Layout;

const Welcome = props => { 

    const [collapsed, setCollapsed] = useState(false)
    const [active_index, setActive_index] = useState(1)
    
     const toggle = () => {
        setCollapsed(!collapsed)
      };

     const changeTab = index => {
        setActive_index(index);
      };

    let id = localStorage.getItem('id');

    console.log(id)

    const history = useHistory()
    
    const cerrarSesion = () => {
      const { confirm } = Modal;
      confirm({
        title: '¿Estás seguro que desea cerrar sesión?',
        icon: <ExclamationCircleOutlined />,
        content: 'Esperamos verte proto en el equipo de Vetec',
        onOk() {
            localStorage.removeItem('token', '');
            localStorage.removeItem('id', '');
            history.push('/');
        },
        onCancel() { },
      });
  
    }

    const IconLink = ({ src, text, links }) => (
        <a className="example-link" href={links} target="_blak">
          <img className="example-link-icon" src={src} alt={text} />
          {text}
        </a>
      );


      const text = <span>Notificaciones</span>;

    const content = () => {
      return(
        <Empty
          image='https://user-images.githubusercontent.com/507615/54591670-ac0a0180-4a65-11e9-846c-e55ffce0fe7b.png'
          imageStyle={{
            height: 60
          }}
          description={
            <span>
              Estas al día, no tienes notificaciones.
             </span>
          }
        />
      )
    }

    return(
    <Query query={ADMIN_DETAIL} variables={{ id }}>
      {({ loading, error, data }) => {
        if (loading) {
          return (
            <div className="page-loader">
              <Spin size="large"></Spin>
            </div>
          );
        }
        if (error){
          console.log('error getting user detail: ', error);
        }
        if(data) {
          const datas = data ? data.getAdmin : ''
      return(
        <Layout style={{ minHeight: '100vh' }}>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo">
            <img src={Logo} className="logo-vetec" />
          </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item 
            key="1"
            className={active_index === 1 ? "active simple" : "simple"}
            onClick={() => changeTab(1)}
            >
                <DashboardOutlined />
              <span>Dashboard</span>
            </Menu.Item>
            <Menu.Item 
            key="2"
            className={active_index === 2 ? "active simple" : "simple"}
            onClick={() => changeTab(2)}
            >
            <UserSwitchOutlined />
              <span>Profesionales</span>
            </Menu.Item>

            <Menu.Item 
            key="3"
            className={active_index === 3 ? "active simple" : "simple"}
            onClick={() => changeTab(3)}
            >
                <UserOutlined />
              <span>Cliente</span>
            </Menu.Item>

            <Menu.Item 
            key="4"
            className={active_index === 4 ? "active simple" : "simple"}
            onClick={() => changeTab(4)}
            >
            <FileDoneOutlined />
              <span>Consultas</span>
            </Menu.Item>

            <Menu.Item 
            key="5"
            className={active_index === 5 ? "active simple" : "simple"}
            onClick={() => changeTab(5)}
            >
            <TeamOutlined />
              <span>Equipo</span>
            </Menu.Item>

            <Menu.Item 
            key="6"
            className={active_index === 6 ? "active simple" : "simple"}
            onClick={() => changeTab(6)}
            >
            <QqOutlined />
              <span>Mascotas</span>
            </Menu.Item>

            <Menu.Item 
            key="7"
            className={active_index === 7 ? "active simple" : "simple"}
            onClick={() => changeTab(7)}
            >
            <EuroOutlined />
              <span>Depositos</span>
            </Menu.Item>

            <Menu.Item 
            key="8"
            className={active_index === 8 ? "active simple" : "simple"}
            onClick={() => changeTab(8)}
            >
            <PercentageOutlined />
              <span>Crear cupón</span>
            </Menu.Item>

            <Menu.Item 
            key="0"
            onClick={()=> cerrarSesion()}
            >
            <LogoutOutlined style={{color: 'red'}}/>
              <span>Cerrar sesión</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
        <PageHeader
            title={`${datas.nombre} ${datas.apellidos}`}
            className="site-layout-background" 
            style={{ padding: 30 }}
            subTitle="soy el"
            tags={<Tag color="blue">{datas.cargo}</Tag>}
            extra={[
            <MenuUnfoldOutlined key="1" style={{fontSize: 28,  marginRight: 15}} onClick={()=>toggle()}/>,
            <Badge count={1} overflowCount={10}>
            <Popover placement="bottom" title={text} content={content} trigger="click" className="Box-notification">
              <BellOutlined key="2" style={{fontSize: 28}}/>
            </Popover>
            </Badge>,
            <Tooltip title="Actualizar perfil">
            <EditOutlined style={{fontSize: 28, marginRight: 15}} onClick={() => changeTab(30)}/>
            </Tooltip>,
            <Tooltip title="Añadir miembro">
            <PlusCircleOutlined style={{fontSize: 28, marginRight: 15}} onClick={() => changeTab(31)}/>
            </Tooltip>,
            <a href="mailto:info@vetec.es" target="blank">
            <QuestionCircleOutlined style={{fontSize: 28, marginRight: 15}}/>
            </a>,
            <DropdownMenu key="more"/>,
            ]}
            avatar={{ src: IMAGES_PATH + datas.avatar }}>
            <Content>
            <>
                <Paragraph className="des">
                    {datas.descricion}
                </Paragraph>
                <div>
                <IconLink
                    src="https://gw.alipayobjects.com/zos/rmsportal/MjEImQtenlyueSmVEfUD.svg"
                    text="Twitter"
                    links={`${datas.twitter}`}
                    key="1"
                />
                <IconLink
                    src="https://gw.alipayobjects.com/zos/rmsportal/NbuDUAuBlIApFuDvWiND.svg"
                    text="Linkedin"
                    links={`${datas.linkedin}`}
                    key="2"
                />
                <IconLink
                    src="https://gw.alipayobjects.com/zos/rmsportal/ohOEPSYdDTNnyMbGuyLb.svg"
                    text="Email"
                    links={`mailto:${datas.email}`}
                    key="3"
                />
                </div>
            </>
            </Content>
        </PageHeader>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            {active_index === 30 ? <MiCuenta data={datas} /> : null}
            {active_index === 31 ? <NewAdmin /> : null}
            {active_index === 1 ? <Dashboard /> : null}
            {active_index === 2 ? <Profesionales /> : null}
            {active_index === 3 ? <Clientes /> : null}
            {active_index === 4 ? <MiConsulta /> : null}
            {active_index === 5 ? <ProfileTeam /> : null}
            {active_index === 6 ? <Mascotas /> : null}
            {active_index === 7 ? <Deposito /> : null}
            {active_index === 8 ? <Cupon /> : null}
          </Content>
        </Layout>
      </Layout>
      )}
      }}
    </Query>
    )
}

export default Welcome;