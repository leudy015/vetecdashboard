import React, { Component } from "react";
import { Form, Input, Button, DatePicker, message } from 'antd';
import { Mutation } from 'react-apollo';
import moment from 'moment';
import { NUEVO_USUARIO_DEPOSITO } from '../../mutations';
import Header from './header';
const dateFormat = 'MM/DD/YYYY'; // compatible con la fecha y hora del servidor para evitar conflictos de API

class Deposito extends Component {

    state = { loading: false }
    render() {
        return (
            <div>
            <Header title="Enviar deposito a profesionales"/>
            <div className="sub-containers">
            <Mutation mutation={NUEVO_USUARIO_DEPOSITO}>
                {(crearDeposito) => (
                    <div style={{ marginTop: 40 }}>
                        <div>
                            <Form
                            name="normal_login"
                            onFinish={ values => {
                              const formSubmit = () => {
                                    this.setState({ loading: true });
                                            const input = {
                                                fecha: moment(values.fecha).format(dateFormat),
                                                estado: values.estado,
                                                total: values.total,
                                                profesionalID: values.profesionalID
                                            };
                                            crearDeposito({ variables: { input } }).then(res => {
                                                if (res.data.crearDeposito.success) {
                                                    message.success(res.data.crearDeposito.message);
                                                    this.setState({ loading: false });
                                                    window.location.reload()
                                                }
                                            }).catch(err => message.error('Algo salió mal. Por favor intente nuevamente en un momento.'))
                                        }
                               
                                formSubmit()
                              }}
                            >
                            <Form.Item
                                name="fecha"
                                rules={[
                                {
                                    required: true,
                                    message: 'Este campo es obligatorio!',
                                },
                                ]}
                            >
                                <DatePicker format={dateFormat} />
                            </Form.Item>
                            <Form.Item
                                name="estado"
                                rules={[
                                {
                                    required: true,
                                    message: 'Este campo es obligatorio!',
                                },
                                ]}
                            >
                            <Input placeholder="Estado" />
                            </Form.Item>
                            <Form.Item
                                name="total"
                                rules={[
                                {
                                    required: true,
                                    message: 'Por favor ingrese su email!',
                                },
                                ]}
                            >
                            <Input placeholder="Total" />
                            </Form.Item>

                            <Form.Item
                                name="profesionalID"
                                rules={[
                                {
                                    required: true,
                                    message: 'Por favor ingrese su email!',
                                },
                                ]}
                            >
                            <Input placeholder="ID del profesional" />
                            </Form.Item>

                            <Form.Item>
                                <Button shape="round" type="primary" htmlType="submit">
                                Guardar
                                </Button>
                            </Form.Item>
                            </Form>
                        </div>
                    </div>
                )}
            </Mutation>
            </div>
            </div>
        )

    }
}

export default Deposito;
