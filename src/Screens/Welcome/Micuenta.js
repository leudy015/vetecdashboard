import React, { useState } from 'react';
import {
    Form,
    Input,
    Button,
    Switch,
    message,
    Upload,
    Tooltip,
    Modal
} from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons'
import {Mutation} from 'react-apollo'
import { IMAGES_PATH } from '../../constant/consfig';
import { ACTUALIZAR_ADMIN, UPLOAD_FILE, ELIMINAR_ADMIN } from '../../mutations/index';


function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

const Micuenta = props => {
    const [notificacion, setNotificacion] = useState(false)
    const [avatar, setAvatar] = useState(props.data.avatar)

    let id = localStorage.getItem('id');

    const datas = props.data

    const onSwitchChange = () => {

        if(notificacion === true){
            setNotificacion(false);
        }else{
            setNotificacion(true);
        }
      };

    const handleEliminar = (eliminarAdmin, id) =>{

        const { confirm } = Modal;

       confirm({
          title: '¿Estás seguro de que deseas eliminar tu cuenta?',
          onOk() {
            return new Promise((resolve, reject) => {
                eliminarAdmin({ variables: { id } }).then(async ({ data }) => {
    
                if (data && data.eliminarAdmin && data.eliminarAdmin.success) {
                  setTimeout(() => {
                    localStorage.removeItem('token', '');
                    localStorage.removeItem('id', '');
    
                    message.success(data.eliminarAdmin.message);
    
                    setTimeout(() => window.location.href = '/register-client', 300);
    
                  }, 200);
                  resolve();
                }
                else if (data && data.eliminarAdmin && !data.eliminarAdmin.success)
                  setTimeout(() => {
                    message.error(data.eliminarAdmin.message);
                  }, 500);
                reject();
              });
            }).catch(() => console.log('error confirming eliminar usuario!'));
          },
          onCancel() { },
        });
    
      }


    const uploadButton = (
        <Tooltip title="Añadir foto del perfil">
            <div>
                <PlusCircleOutlined />
                <div className="ant-upload-text">Añadir foto del perfil</div>
            </div>
        </Tooltip>
        )    

    return (
        <Mutation mutation={ACTUALIZAR_ADMIN}>
        {(actualizaradmin) => (
        <div className="sub-containers">
        <div className="CardForm">
            <Form.Item>
                    <Mutation mutation={UPLOAD_FILE}>
                        {(singleUpload) => (
                            <Upload
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                customRequest={async (data) => {
                                    let imgBlob = await getBase64(data.file);
                                    singleUpload({ variables: { imgBlob } }).then((res) => {
                                        setAvatar(res && res.data && res.data.singleUpload ? res.data.singleUpload.filename : '')
                                    }).catch(error => {
                                        console.log('fs error: ', error)
                                    })
                                }}
                            >
                            {
                                avatar ? 
                                <Tooltip title="Actualizar foto del perfil">
                                <img src={IMAGES_PATH + avatar} alt="avatar" style={{ width: '100%' }} /> 
                                </Tooltip>
                                : null

                            }

                            {
                                !avatar ?
                                uploadButton : null
                            }
                            
                            </Upload>
                        )}
                    </Mutation>
                
            </Form.Item>
            <Form
                layout="horizontal"
                size="large"
                onFinish={ values => {
                      console.log('in render values: ', values);
                        const input = {
                          id: localStorage.getItem('id'),
                          email: values.email,
                          nombre: values.nombre,
                          apellidos: values.apellidos,
                          ciudad: values.ciudad,
                          telefono: values.telefono,
                          cargo: values.cargo,
                          twitter: values.twitter,
                          linkedin: values.linkedin,
                          descricion: values.descricion,
                          avatar: avatar,
                          notificacion: notificacion,
                        };
                        console.log('input; ', input)
                        actualizaradmin({ variables: { input } }).then(res => {
                          message.success('El perfil ha sido actualizado exitosamente');
                        }).catch(err => message.error('Algo salió mal. Por favor, vuelva a intentarlo'))
                  }}
                 >
                <Form.Item
                    name="nombre"
                >
                    <Input
                        placeholder="Nombre"
                        defaultValue={datas.nombre}
                    />
                </Form.Item>
                <Form.Item
                    name="apellidos"
                >
                    <Input
                        placeholder="Apellidos"
                        defaultValue={datas.apellidos}
                    />
                </Form.Item>
                <Form.Item
                    name="email"
                >
                    <Input
                        placeholder="Email"
                        defaultValue={datas.email}
                    />
                </Form.Item>
                <Form.Item
                    name="telefono"
                >
                    <Input
                        placeholder="Número móvil"
                        defaultValue={datas.telefono}
                    />
                </Form.Item>

                <Form.Item
                    name="ciudad"
                >
                    <Input
                        placeholder="Ciudad"
                        defaultValue={datas.ciudad}  
                    />
                </Form.Item>

                <Form.Item
                name="cargo"
                    >
                        <Input
                            placeholder="¿Cúal es tu cargo?"
                            defaultValue={datas.cargo}  
                        />
                    </Form.Item>

                    <Form.Item
                    name="linkedin"
                    >
                    <Input
                        placeholder="Linkedin"
                        defaultValue={datas.linkedin}  
                    />
                    </Form.Item>

                    <Form.Item
                    name="twitter"
                    >
                    <Input
                        placeholder="Twitter"
                        defaultValue={datas.twitter}  
                    />
                    </Form.Item>

                    <Form.Item
                    name="descricion"
                    >
                    <Input
                        placeholder="Descripción"
                        defaultValue={datas.descricion}  
                    />
                    </Form.Item>

                <Form.Item label="Activar las notificaciones">
                    <Switch 
                    defaultChecked={datas.notificacion}
                    onChange={() => onSwitchChange()}
                    />
                </Form.Item>
                <Form.Item>
                    <Button shape="round" htmlType="submit" type="primary">Guardar cambios</Button>
                </Form.Item>
                <Form.Item>
                <Mutation mutation={ELIMINAR_ADMIN}>
                    {(eliminarAdmin) => {
                        return (
                    <Button onClick={() => handleEliminar(eliminarAdmin, id)} type="danger" shape="round">
                        Eliminar cuenta
                    </Button>
                          )
                        }}
                    </Mutation>
                </Form.Item>
            </Form>
        </div>
    </div>
    )}
</Mutation>
    );
};


export default Micuenta;