import React, {useState} from 'react';
import { Table, Tag, Spin, Modal, message, Button} from 'antd';
import Header from './header';
import { GET_ALL_CUPON } from '../../query/index';
import { ELIMINAR_CUPON } from '../../mutations/index';
import { Query, Mutation } from 'react-apollo';
import {ExclamationCircleOutlined, PlusCircleOutlined} from '@ant-design/icons'
import Cupon from './cupon';



const TodoCupones = props =>{

    const [ visible, setVisible ] = useState (false)
    const [refetch, setRefetch] = useState (null)


    const handleEliminar = (eliminarCupon, id) =>{
      const { confirm } = Modal;
      confirm({
          title: '¿Estás seguro que desea eliminar este cupón?',
          icon: <ExclamationCircleOutlined />,
          content: 'Al eliminar este cupon perderemos toda la información relacionada a el',
        onOk() {
          return new Promise((resolve, reject) => {
              eliminarCupon({ variables: { id } }).then(async ({ data }) => {
              if (data && data.eliminarCupon && data.eliminarCupon.success) {
              message.success(data.eliminarCupon.message);
                resolve();
                window.location.reload()
              }
              else if (data && data.eliminarCupon && !data.eliminarCupon.success)
                setTimeout(() => {
                  message.error(data.eliminarCupon.message);
                }, 500);
              reject();
            });
          }).catch(() => console.log('error confirming eliminar cupon!'));
        },
        onCancel() {},
      });
  
    }
  
  const columns = [
    {
      title: 'Tipo',
      dataIndex: 'tipo',
      key: 'tipo',
      render: (text) => <a>{text}</a>,
    },
    {
      title: 'Cantidad',
      dataIndex: 'descuento',
      key: 'descuento',
      
    },
    {
      title: 'Clave',
      key: 'clave',
      dataIndex: 'clave',
      render: (clave) => (
        <span>
          <Tag color="geekblue">
              {clave}
          </Tag>
        </span>
      ),
    },
    {
      title: 'Eliminar',
      key: 'dataIndex',
      render: (dataIndex) => (
        <span>
        <Mutation mutation={ELIMINAR_CUPON}>
        {(eliminarCupon, refetch) => {
          return (
      <Button
        type="danger"
        className="btnflex"
        shape="round"
        onClick={() => handleEliminar(eliminarCupon, dataIndex.id)}
      >
        Eliminar cupón
      </Button>
      )
        }}
    </Mutation>
        </span>
      ),
    },
  ];
  

  const showModal = () => {
       setVisible(true)
      };

const handleCancel = () => {
    setVisible(false)
}
 
      
    return(
        <Query query={GET_ALL_CUPON}>
      {(response, error, loading, refetch) => {
        setRefetch(refetch)
        if (loading) {
          return <div className="page-loader">
            <Spin size="large"></Spin>
          </div>
        }
        if (error) {
          return console.log('Response Error-------', error);
        }
        if (response) {
          const datas = response && response.data ? response.data.getAllCupon : ""
          console.log(response)
          return (
        <div>
        <Header 
        title="Administrar cupones"
        tit="cupones totales"
        total={datas.length}
        extra={ 
            <Button type="primary" shape="round" icon={<PlusCircleOutlined />} size='large' onClick={()=> showModal()}>Añadir cupón</Button>
        }
        />
        <Table
          columns={columns}
          dataSource={datas}
          rowKey={data => data.id}
          loading={props.dataLoading}
          scroll={true}
          className="Tabs"
        />
        <Modal
        title="Añadir Cupón"
        visible={visible}
        footer={false}
        onCancel={() => handleCancel()}
      >
        <Cupon />
      </Modal>

      </div>
      )
        }
      }}
    </Query>
    )
}

export default TodoCupones;