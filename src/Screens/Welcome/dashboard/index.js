import React from 'react'
import Page2 from './Page2';
import Grafico from './gafico1'
import Graficoq from './Graficoq'
import { Row, Col} from 'antd';

const Dashboard = props => {
    return (
        <div className="containers-das">
            <Page2 />
            <Row>
                <Col span={10} style={{ marginTop: 50, marginRight: 100 }}>
                    <Grafico />
                </Col>
                    <Col span={10} style={{ marginTop: 50 }}>
                    <Graficoq />
                </Col>
            </Row>
            
        </div>
    )
}

export default Dashboard;