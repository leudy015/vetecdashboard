import React from 'react';
import { Statistic, Card, Row, Col } from 'antd';
import { ArrowUpOutlined } from '@ant-design/icons';
import './index.css'



const Page2 = props =>{
    return(
        <div className="site-statistic-demo-card">
        <Row gutter={16}>
        <Col span={6}>
            <Card>
            <Statistic
                title="Nuevos usuarios"
                value={11.28}
                precision={2}
                valueStyle={{ color: '#3f8600' }}
                prefix={<ArrowUpOutlined />}
            />
            </Card>
        </Col>
        <Col span={6}>
            <Card>
            <Statistic
                title="Profesionales"
                value={9.3}
                precision={2}
                valueStyle={{ color: '#3f8600' }}
                prefix={<ArrowUpOutlined />}
            />
            </Card>
        </Col>
        <Col span={6}>
        <Card>
        <Statistic
            title="Consultas"
            value={9.3}
            precision={2}
            valueStyle={{ color: '#3f8600' }}
            prefix={<ArrowUpOutlined />}
        />
        </Card>
    </Col>
    <Col span={6}>
    <Card>
    <Statistic
        title="Crecimiento"
        value={9.3}
        precision={2}
        valueStyle={{ color: '#3f8600' }}
        prefix={<ArrowUpOutlined />}
        suffix="%"
    />
    </Card>
        </Col>
        </Row>
    </div>
    )
}

export default Page2