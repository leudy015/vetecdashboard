import  React  from 'react';
import { PageHeader, Tag, Button, Statistic, Row } from 'antd';


const Header = props => {
    return (
        <PageHeader
        onBack={() => window.history.back()}
        style={{marginBottom: 30}}
        title={props.title}
        extra={props.extra}
      >
        <Row>
        {
          props.total ?
          <Statistic
            title={props.tit}
            value={props.total}
            style={{
              margin: '0 32px',
            }}
          /> : null
        }
          
        </Row>
      </PageHeader>
    )
}

export default Header;


