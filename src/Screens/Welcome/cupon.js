import React, { Component } from "react";
import { Form, Input, Button, Select, message, InputNumber } from 'antd';
import { Mutation } from 'react-apollo';
import { CREAR_CUPON } from '../../mutations';

const { Option } = Select;

class Cupon extends Component {

    state = { loading: false }

    render() {
        return (
            <div className="sub-containers">
            <Mutation mutation={CREAR_CUPON}>
                {(crearCupon) => (
                    <div style={{ marginTop: 40 }}>
                        <div>
                            <Form
                            name="normal_login"
                            onFinish={ values => {
                              const formSubmit = () => {
                                    this.setState({ loading: true });
                                            const input = {
                                                clave: values.clave,
                                                descuento: values.descuento,
                                                tipo: values.tipo
                                            };
                                            console.log(input)
                                            crearCupon({ variables: { input } }).then(res => {
                                                    message.success(res.data.crearCupon.message);
                                                    window.location.reload()
                                                    this.setState({ loading: false });
                                            
                                            }).catch(err => message.error('Algo salió mal. Por favor intente nuevamente en un momento.'))
                                        }
                               
                                formSubmit()
                              }}
                            >
                            <Form.Item
                                name="clave"
                                rules={[
                                {
                                    required: true,
                                    message: 'Este campo es obligatorio!',
                                },
                                ]}
                            >
                            <Input placeholder="Clave" />
                            </Form.Item>
                            <Form.Item
                                name="descuento"
                                rules={[
                                {
                                    required: true,
                                    message: 'Por favor ingrese su email!',
                                },
                                ]}
                            >
                            <InputNumber min={1} max={100} defaultValue={5} />
                            </Form.Item>

                            <Form.Item
                                name="tipo"
                                rules={[
                                {
                                    required: true,
                                    message: 'Este campo es obligatorio!',
                                },
                                ]}
                            >
                            <Select>
                                <Option value="porcentaje">Porcentaje</Option>
                                <Option value="dinero">Dinero</Option>
                            </Select>
                            </Form.Item>

                            <Form.Item>
                                <Button shape="round" type="primary" htmlType="submit">
                                    Guardar cupón
                                </Button>
                            </Form.Item>
                            </Form>
                        </div>
                    </div>
                )}
            </Mutation>
            </div>
        )

    }
}

export default Cupon;
