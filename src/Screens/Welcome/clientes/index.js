import React, { useState } from "react";
import {
  Table,
  Tag,
  Input,
  Spin,
  Button,
  Modal,
  message,
  Avatar,
  Descriptions,
  Select,
  Result,
} from "antd";
import Header from "../header";
import { Query, Mutation } from "react-apollo";
import {
  GET_CLIENTE_SEARCH,
  CONSULTA_BY_USUARIO,
  GET_MASCOTAS,
} from "../../../query/index";
import { ELIMINAR_USUARIO } from "../../../mutations/index";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import "../index.css";
import { IMAGES_PATH } from "../../../constant/consfig";
import moment from "moment";
import "moment/locale/es";

const { Search } = Input;
const { Option } = Select;

const Clientes = () => {
  const [search, setSearch] = useState("");
  const [visible, setVisible] = useState(false);
  const [dat, setDat] = useState(null);
  const [consultaID, setConsultaID] = useState(null);
  const [MAscotaID, setMAscotaID] = useState(null);

  let refetch;

  function onChange(value) {
    console.log(`selected ${value}`);
    setConsultaID(value);
  }

  function onChangemas(value) {
    console.log(`selected ${value}`);
    setMAscotaID(value);
  }

  function onBlur() {
    console.log("blur");
  }

  function onFocus() {
    console.log("focus");
  }

  function onSearch(val) {
    console.log("search:", val);
  }

  const showModal = (dataIndex) => {
    setDat(dataIndex);
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const handleEliminar = (eliminarUsuario, id) => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que desea eliminar este cliente?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Al eliminar este cliente perderemos toda la información relacionada a el",
      onOk() {
        return new Promise((resolve, reject) => {
          eliminarUsuario({ variables: { id } }).then(async ({ data }) => {
            if (data && data.eliminarUsuario && data.eliminarUsuario.success) {
              message.success(data.eliminarUsuario.message);
              resolve();
              refetch();
            } else if (
              data &&
              data.eliminarUsuario &&
              !data.eliminarUsuario.success
            )
              setTimeout(() => {
                message.error(data.eliminarUsuario.message);
              }, 500);
            reject();
          });
        }).catch(() => console.log("error confirming eliminar usuario!"));
      },
      onCancel() {},
    });
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
      render: (text) => <p>{text}</p>,
    },
    {
      title: "Nombre",
      dataIndex: "nombre",
      key: "nombre",
      render: (text) => <p>{text} </p>,
    },
    {
      title: "Apellidos",
      dataIndex: "apellidos",
      key: "apellidos",
      render: (text) => <p>{text}</p>,
    },

    {
      title: "Ciudad",
      dataIndex: "ciudad",
      key: "ciudad",
      render: (text) => <p>{text}</p>,
    },
    {
      title: "Profesión",
      key: "profesion",
      dataIndex: "profesion",
      render: (tags) => <Tag color="geekblue">{tags}</Tag>,
    },
    {
      title: "Detalles",
      key: "x",
      render: (dataIndex) => {
        return (
          <span className="btnflex">
            <Button
              type="primary"
              shape="round"
              className="btnflex"
              style={{ marginRight: 25 }}
              onClick={() => showModal(dataIndex)}
            >
              Ver Detalles
            </Button>
            <Mutation mutation={ELIMINAR_USUARIO}>
              {(eliminarUsuario) => {
                return (
                  <Button
                    type="danger"
                    shape="round"
                    className="btnflex"
                    onClick={() =>
                      handleEliminar(eliminarUsuario, dataIndex.id)
                    }
                  >
                    Eliminar
                  </Button>
                );
              }}
            </Mutation>
          </span>
        );
      },
    },
  ];

  return (
    <Query query={GET_CLIENTE_SEARCH} variables={{ search: search }}>
      {(response) => {
        refetch = response.refetch;
        if (response.error) {
          return (
            <div className="page-loader">
              <Spin size="large"></Spin>
            </div>
          );
        }
        if (response) {
          const datas =
            response && response.data && response.data.getClientesSearchadmin
              ? response.data.getClientesSearchadmin.data
              : "";
          return (
            <div>
              <Header
                title="Clientes"
                extra={
                  <Search
                    placeholder="Buscar cliente"
                    style={{ width: 500 }}
                    enterButton="Buscar"
                    size="large"
                    onSearch={(value) => setSearch(value)}
                  />
                }
                tit="Total de clientes"
                total={datas.length}
              />
              <Table
                columns={columns}
                dataSource={datas}
                rowKey={(data) => data.id}
                loading={response.loading}
                scroll={true}
                className="Tabs"
              />

              {dat ? (
                <Modal
                  title={`${dat.nombre} ${dat.apellidos}`}
                  visible={visible}
                  footer={false}
                  onCancel={() => handleCancel()}
                  style={{ width: "70%" }}
                >
                  <div className="content-modal">
                    <Avatar src={IMAGES_PATH + dat.avatar} size={90} />
                    <div
                      style={{
                        display: "flex",
                        textAlign: "center",
                        alignSelf: "center",
                        justifyContent: "center",
                        marginTop: 10,
                      }}
                    >
                      <h2>
                        {dat.nombre} {dat.apellidos}
                      </h2>
                    </div>
                    <div style={{ textAlign: "left" }}>
                      <Descriptions title="Contacto" layout="vertical">
                        <Descriptions.Item label="Telefono">
                          {dat.telefono}
                        </Descriptions.Item>
                        <Descriptions.Item label="Email">
                          {dat.email}
                        </Descriptions.Item>
                      </Descriptions>
                      <Descriptions title="Detalles" layout="vertical">
                        <Descriptions.Item label="Cliente desde">
                          {moment(Number(dat.created_at)).format("L")}
                        </Descriptions.Item>
                      </Descriptions>
                    </div>
                    <Query
                      query={CONSULTA_BY_USUARIO}
                      variables={{ usuarios: dat.id }}
                    >
                      {(response) => {
                        if (response.loading) {
                          return (
                            <div className="page-loader">
                              <Spin size="large"></Spin>
                            </div>
                          );
                        }
                        if (response) {
                          const datass =
                            response &&
                            response.data &&
                            response.data.getConsultaByUsuario
                              ? response.data.getConsultaByUsuario.list
                              : "";
                          return (
                            <div style={{ textAlign: "left", marginTop: 20 }}>
                              <h3>Consultas realizada por este cliente</h3>
                              {datass.length > 0 ? (
                                <div>
                                  <Select
                                    showSearch
                                    style={{ width: "100%", marginBottom: 50 }}
                                    placeholder="Buscar consultas"
                                    optionFilterProp="children"
                                    onChange={onChange}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    onSearch={onSearch}
                                    filterOption={(input, option) =>
                                      option.children
                                        .toLowerCase()
                                        .indexOf(input.toLowerCase()) >= 0
                                    }
                                  >
                                    {datass.map((mas, index) => (
                                      <Option key={index} value={mas.id}>{`${
                                        mas && mas.profesional
                                          ? mas.profesional.nombre
                                          : ""
                                      } ${
                                        mas && mas.profesional
                                          ? mas.profesional.apellidos
                                          : ""
                                      }  De fecha  ${moment(
                                        Number(mas.created_at)
                                      ).format("LL")}`}</Option>
                                    ))}
                                  </Select>
                                </div>
                              ) : null}

                              {consultaID ? (
                                <div>
                                  <h3>ID de la consulta seleccionada</h3>
                                  <p>{consultaID}</p>
                                </div>
                              ) : null}

                              {datass &&
                              ((datass && datass.length === 0) || !datass) ? (
                                <div>
                                  <Result
                                    status="404"
                                    title="Aún no ha realizado consultas"
                                    subTitle="Este cliente no ha realizado ningúna consulta."
                                  />
                                </div>
                              ) : null}
                            </div>
                          );
                        }
                      }}
                    </Query>

                    <Query
                      query={GET_MASCOTAS}
                      variables={{ usuarios: dat.id }}
                    >
                      {({ loading, error, data, refetch }) => {
                        refetch = refetch;
                        if (loading) {
                          return (
                            <div className="page-loader">
                              <Spin size="large"></Spin>
                            </div>
                          );
                        }
                        if (error) {
                          return console.log("error in mascota", error);
                        }
                        if (data) {
                          const datas =
                            data && data.getMascota ? data.getMascota.list : "";
                          return (
                            <div style={{ textAlign: "left", marginTop: 20 }}>
                              <h3>Mascotas del cliente</h3>
                              {datas.length > 0 ? (
                                <div>
                                  <Select
                                    showSearch
                                    style={{ width: "100%", marginBottom: 50 }}
                                    placeholder="Selecciona una mascota"
                                    optionFilterProp="children"
                                    onChange={onChangemas}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    onSearch={onSearch}
                                    filterOption={(input, option) =>
                                      option.children
                                        .toLowerCase()
                                        .indexOf(input.toLowerCase()) >= 0
                                    }
                                  >
                                    {datas.map((mas, i) => (
                                      <Option key={i} value={mas.id}>
                                        {mas.name}
                                      </Option>
                                    ))}
                                  </Select>
                                </div>
                              ) : null}

                              {MAscotaID ? (
                                <div>
                                  <h3>ID de la mascota seleccionada</h3>
                                  <p>{MAscotaID}</p>
                                </div>
                              ) : null}

                              {datas &&
                              ((datas && datas.length === 0) || !datas) ? (
                                <div>
                                  <Result
                                    status="404"
                                    title="Aún no ha agregado mascotas"
                                    subTitle="Este cliente no ha agregado ningúna mascota."
                                  />
                                </div>
                              ) : null}
                            </div>
                          );
                        } else {
                          return null;
                        }
                      }}
                    </Query>
                  </div>
                </Modal>
              ) : null}
            </div>
          );
        }
      }}
    </Query>
  );
};

export default Clientes;
