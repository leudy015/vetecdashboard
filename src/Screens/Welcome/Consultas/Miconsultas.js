import React, { useState } from "react";
import "./index.css";
import ConsultasTabs from "./CardConsulta";
import { CONSULTA_BY_ADMIN } from "../../../query";
import { Query } from "react-apollo";
import { Spin, DatePicker, Button, Input } from "antd";
import moment from "moment";
import locate from "moment/locale/es";
import Header from "../header";

const { RangePicker } = DatePicker;

const { Search } = Input;

const MiConsulta = (props) => {
  const date = new Date();
  let day = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();

  const dateFormat = "MM/DD/YYYY";
  const fromDate = `${month}/${day}/${year}`;
  const toDate = "12/31/2020";

  const [dateRange, setDateRange] = useState({
    fromDate: fromDate,
    toDate: toDate,
  });
  const [search, setSearch] = useState("");

  const onDateRangeChange = (dates, dateStrings) => {
    const fromDate = dateStrings[0];
    const toDate = dateStrings[1];
    setDateRange({ fromDate: fromDate, toDate: toDate });
  };

  const aplicarDateRange = () => {
    setDateRange({ fromDate: fromDate, toDate: toDate });
  };

  const deleteDateRange = () => {
    setDateRange(null);
  };

  return (
    <Query
      query={CONSULTA_BY_ADMIN}
      variables={{ search: search, dateRange: dateRange }}
    >
      {(response) => {
        if (response.error) {
          return (
            <div className="page-loader">
              <Spin size="large"></Spin>
            </div>
          );
        }
        if (response) {
          const datas =
            response && response.data && response.data.getConsultaadmin
              ? response.data.getConsultaadmin.list
              : "";
          return (
            <div>
              <Header
                title="Consultas"
                extra={
                  <Search
                    placeholder="Buscar consulta por ID"
                    style={{ width: 500 }}
                    enterButton="Buscar"
                    size="large"
                    onSearch={(value) => setSearch(value)}
                  />
                }
                tit="Total de consultas"
                total={datas.length}
              />
              <div>
                Filtrar por fecha <br />
                <RangePicker
                  locale={locate}
                  defaultValue={[
                    moment(fromDate, dateFormat),
                    moment(toDate, dateFormat),
                  ]}
                  format={dateFormat}
                  onChange={onDateRangeChange}
                />
                <Button
                  onClick={() => aplicarDateRange()}
                  style={{ marginBottom: 20, marginTop: 10, marginLeft: 5 }}
                  type="dashed"
                >
                  Aplicar
                </Button>
                <Button
                  onClick={() => deleteDateRange()}
                  style={{ marginBottom: 20, marginTop: 10, marginLeft: 15 }}
                  type="dashed"
                >
                  Ver todas las consultas
                </Button>
              </div>
              <ConsultasTabs
                data={datas}
                refetch={response.refetch}
                dataLoading={response.loading}
              />
            </div>
          );
        }
      }}
    </Query>
  );
};

export default MiConsulta;
