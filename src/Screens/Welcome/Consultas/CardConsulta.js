import React, { useState } from "react";
import {
  Table,
  Descriptions,
  Avatar,
  Tabs,
  Button,
  message,
  Modal,
} from "antd";
import moment from "moment";
import "moment/locale/es";
import { IMAGES_PATH, LOCAL_API_URL } from "../../../constant/consfig";
import { useMutation } from "@apollo/react-hooks";
import {
  CREATE_NOTIFICATION,
  PROFESSIONAL_CONSULTA_PROCEED,
  ELIMINAR_CONSULTA,
} from "../../../mutations";
import { Mutation, withApollo } from "react-apollo";
import "./index.css";
import { ExclamationCircleOutlined } from "@ant-design/icons";

const { TabPane } = Tabs;

const { confirm } = Modal;

const ConsultasTabs = (props) => {
  const [messageTexte] = useState(
    "La consulta ha sido devuelta en unos tres días naturales te haremos el ingreso"
  );
  const [messageTextepro] = useState(
    "La consulta ha sido marcada como devuelta"
  );
  const [createNotification] = useMutation(CREATE_NOTIFICATION);

  const showConfirm = async (dataIndex, consultaProceed) => {
    confirm({
      title: "Do you Want to delete these items?",
      icon: <ExclamationCircleOutlined />,
      content: "Some descriptions",
      onOk() {
        const formData = {
          consultaID: dataIndex.id,
          estado: "Devuelto",
          progreso: "0",
          status: "exception",
        };
        consultaProceed({ variables: formData })
          .then(async (res) => {
            if (
              res &&
              res.data &&
              res.data.consultaProceed &&
              res.data.consultaProceed.success
            ) {
              message.success("La consulta ha sido devuelta éxistosamente");
              console.log("La consulta ha sido devuelta éxistosamente", res);
              props.refetch();
              /////////////profesional///////////////
              const NotificationInput = {
                consulta: dataIndex.id,
                user: dataIndex.profesional.id,
                usuario: dataIndex.usuario.id,
                profesional: dataIndex.profesional.id,
                type: "consulta_devuelta",
              };
              createNotification({ variables: { input: NotificationInput } })
                .then(async (results) => {
                  console.log("results", results);
                })
                .catch((err) => {
                  console.log("err", err);
                });

              const SendPushNotificationvalorar = () => {
                fetch(
                  `${LOCAL_API_URL}/send-push-notification-profesional?IdOnesignal=${dataIndex.profesional.UserID}&textmessage=${messageTextepro}`
                ).catch((err) => console.log(err));
              };

              SendPushNotificationvalorar();
              /////////////cliente///////////////
              const NotificationInputcli = {
                consulta: dataIndex.id,
                user: dataIndex.usuario.id,
                usuario: dataIndex.usuario.id,
                profesional: dataIndex.profesional.id,
                type: "consulta_devuelta",
              };
              createNotification({ variables: { input: NotificationInputcli } })
                .then(async (results) => {
                  console.log("results", results);
                })
                .catch((err) => {
                  console.log("err", err);
                });

              const SendPushNotificationvalorarclien = () => {
                fetch(
                  `${LOCAL_API_URL}/send-push-notification?IdOnesignal=${dataIndex.usuario.UserID}&textmessage=${messageTexte}`
                ).catch((err) => console.log(err));
              };

              SendPushNotificationvalorarclien();
            }
          })
          .catch((err) => {
            console.log(
              "Algo salió mal. Por favor intente nuevamente en un momento.",
              err
            );
          });
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const handleEliminar = (eliminarconsulta, id) => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que desea eliminar esta consulta?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Al eliminar esta consulta perderemos toda la información relacionada a ella",
      onOk() {
        return new Promise((resolve, reject) => {
          eliminarconsulta({ variables: { id } }).then(async ({ data }) => {
            if (
              data &&
              data.eliminarconsulta &&
              data.eliminarconsulta.success
            ) {
              message.success(data.eliminarconsulta.message);
              resolve();
              props.refetch();
            } else if (
              data &&
              data.eliminarconsulta &&
              !data.eliminarconsulta.success
            )
              setTimeout(() => {
                message.error(data.eliminarconsulta.message);
              }, 500);
            reject();
          });
        }).catch(() => console.log("error confirming eliminar usuario!"));
      },
      onCancel() {},
    });
  };

  const columns = [
    {
      title: "Profesional",
      dataIndex: "profesional",
      render: (profesional) => profesional.nombre,
    },
    {
      title: "Total",
      dataIndex: "profesional",
      render: (profesional) => profesional.Precio + "€",
    },
    {
      title: "Estado de la consulta",
      dataIndex: "estado",
      key: "estado",
    },
    {
      title: "Fecha",
      render: (data) => moment(Number(data.created_at)).format("llll"),
      key: "created_at",
    },
    {
      title: "Iniciar",
      key: "x",
      render: (dataIndex) => {
        return (
          <span className="btnflex">
            {dataIndex && dataIndex.estado !== "Devuelto" ? (
              <Mutation mutation={PROFESSIONAL_CONSULTA_PROCEED}>
                {(consultaProceed) => (
                  <Button
                    type="secondary"
                    className="btnflex"
                    shape="round"
                    style={{ marginRight: 20 }}
                    onClick={() => showConfirm(dataIndex, consultaProceed)}
                  >
                    Hacer devolución
                  </Button>
                )}
              </Mutation>
            ) : (
              ""
            )}

            <Mutation mutation={ELIMINAR_CONSULTA}>
              {(eliminarconsulta) => {
                return (
                  <Button
                    type="danger"
                    className="btnflex"
                    shape="round"
                    onClick={() =>
                      handleEliminar(eliminarconsulta, dataIndex.id)
                    }
                  >
                    Eliminar consulta
                  </Button>
                );
              }}
            </Mutation>
          </span>
        );
      },
    },
  ];

  return (
    <div>
      <Table
        columns={columns}
        rowKey={(data) => data.id}
        loading={props.dataLoading}
        scroll={true}
        className="Tabs"
        expandable={{
          expandedRowRender: (record) => {
            return (
              <div>
                <Tabs defaultActiveKey="1">
                  <TabPane
                    tab={
                      <span>
                        <Avatar
                          src={IMAGES_PATH + record.mascota.avatar}
                          style={{ marginRight: 10 }}
                        />
                        {record.mascota.name}
                      </span>
                    }
                    key="1"
                  >
                    <Avatar src={IMAGES_PATH + record.mascota.avatar} />
                    <Descriptions title={record.mascota.name}>
                      <Descriptions.Item label="Especie">
                        {record.mascota.especie}
                      </Descriptions.Item>
                      <Descriptions.Item label="Raza">
                        {record.mascota.raza}
                      </Descriptions.Item>
                      <Descriptions.Item label="Genero">
                        {record.mascota.genero}
                      </Descriptions.Item>
                      <Descriptions.Item label="Peso">
                        {record.mascota.peso} KG
                      </Descriptions.Item>
                      <Descriptions.Item label="Alergias">
                        {record.mascota.alergias}
                      </Descriptions.Item>
                      <Descriptions.Item label="Vacunas">
                        {record.mascota.vacunas}
                      </Descriptions.Item>
                    </Descriptions>
                  </TabPane>
                  <TabPane
                    tab={
                      <span>
                        <Avatar
                          src={IMAGES_PATH + record.profesional.avatar}
                          style={{ marginRight: 10 }}
                        />
                        {`${record.profesional.nombre} ${record.profesional.apellidos}`}
                      </span>
                    }
                    key="2"
                  >
                    <Avatar src={IMAGES_PATH + record.profesional.avatar} />
                    <Descriptions
                      title={`${record.profesional.nombre} ${record.profesional.apellidos}`}
                    >
                      <Descriptions.Item label="Experiencia">
                        {record.profesional.experiencia}
                      </Descriptions.Item>
                      <Descriptions.Item label="Ciudad">
                        {record.profesional.ciudad}
                      </Descriptions.Item>
                      <Descriptions.Item label="Profesión">
                        {record.profesional.profesion}
                      </Descriptions.Item>
                      <Descriptions.Item label="Descripción">
                        {record.profesional.descricion}
                      </Descriptions.Item>
                    </Descriptions>
                  </TabPane>
                  <TabPane
                    tab={
                      <span>
                        <Avatar
                          src={IMAGES_PATH + record.usuario.avatar}
                          style={{ marginRight: 10 }}
                        />
                        {`${record.usuario.nombre} ${record.usuario.apellidos}`}
                      </span>
                    }
                    key="3"
                  >
                    <Avatar src={IMAGES_PATH + record.usuario.avatar} />
                    <Descriptions
                      title={`${record.usuario.nombre} ${record.usuario.apellidos}`}
                    >
                      <Descriptions.Item label="Telefono">
                        {record.usuario.telefono}
                      </Descriptions.Item>
                      <Descriptions.Item label="Ciudad">
                        {record.usuario.ciudad}
                      </Descriptions.Item>
                      <Descriptions.Item label="Email">
                        {record.usuario.email}
                      </Descriptions.Item>
                    </Descriptions>
                  </TabPane>
                </Tabs>
              </div>
            );
          },
          rowExpandable: (record) =>
            record.profesional.nombre !== "Not Expandable",
        }}
        dataSource={props.data}
      />
    </div>
  );
};

export default withApollo(ConsultasTabs);
