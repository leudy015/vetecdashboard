import React, { useState } from "react";
import {
  Table,
  Tag,
  Input,
  Spin,
  Button,
  Modal,
  message,
  Avatar,
  Descriptions,
  List,
} from "antd";
import Header from "../header";
import { Query, Mutation } from "react-apollo";
import { GET_MASCOTAS_ADMIN, GET_DIAGNOSTICO } from "../../../query/index";
import { ELIMINAR_MASCOTA } from "../../../mutations/index";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import "../index.css";
import { IMAGES_PATH } from "../../../constant/consfig";
import moment from "moment";
import "moment/locale/es";

const { Search } = Input;

const Mascotas = () => {
  const [search, setSearch] = useState("");
  const [visible, setVisible] = useState(false);
  const [dat, setDat] = useState(null);

  const showModal = (dataIndex) => {
    setDat(dataIndex);
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const handleEliminar = (eliminarMascota, id) => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que desea eliminar esta mascota?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Al eliminar esta mascota perderemos toda la iinformación relacionada con ella",
      onOk() {
        return new Promise((resolve, reject) => {
          eliminarMascota({ variables: { id } }).then(async ({ data }) => {
            if (data && data.eliminarMascota && data.eliminarMascota.success) {
              message.success(data.eliminarMascota.message);
              resolve();
              window.location.reload();
            } else if (
              data &&
              data.eliminarMascota &&
              !data.eliminarMascota.success
            )
              setTimeout(() => {
                message.error(data.eliminarMascota.message);
              }, 500);
            reject();
          });
        }).catch(() => console.log("error confirming eliminar mascota!"));
      },
      onCancel() {},
    });
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
      render: (text) => <p>{text}</p>,
    },
    {
      title: "Nombre",
      dataIndex: "name",
      key: "name",
      render: (text) => <p>{text} </p>,
    },

    {
      title: "Dueño",
      dataIndex: "usuario",
      key: "usuario",
      render: (text) => <p>{text}</p>,
    },
    {
      title: "Edad",
      key: "age",
      dataIndex: "age",
      render: (tags) => <Tag color="geekblue">{moment(tags).format("LL")}</Tag>,
    },
    {
      title: "Detalles",
      key: "x",
      render: (dataIndex) => {
        return (
          <span className="btnflex">
            <Button
              type="primary"
              shape="round"
              className="btnflex"
              style={{ marginRight: 25 }}
              onClick={() => showModal(dataIndex)}
            >
              Ver Detalles
            </Button>
            <Mutation mutation={ELIMINAR_MASCOTA}>
              {(eliminarMascota) => {
                return (
                  <Button
                    type="danger"
                    shape="round"
                    className="btnflex"
                    onClick={() =>
                      handleEliminar(eliminarMascota, dataIndex.id)
                    }
                  >
                    Eliminar
                  </Button>
                );
              }}
            </Mutation>
          </span>
        );
      },
    },
  ];

  return (
    <Query query={GET_MASCOTAS_ADMIN} variables={{ id: search }}>
      {(response, error, loading) => {
        if (loading) {
          return (
            <div className="page-loader">
              <Spin size="large"></Spin>
            </div>
          );
        }
        if (error) {
          return console.log("Response Error-------", error);
        }
        if (response) {
          const datas =
            response && response.data && response.data.getMascotaadmin
              ? response.data.getMascotaadmin.list
              : "";
          return (
            <div>
              <Header
                title="Mascotas"
                extra={
                  <Search
                    placeholder="Buscar cliente"
                    style={{ width: 500 }}
                    enterButton="Buscar"
                    size="large"
                    onSearch={(value) => setSearch(value)}
                  />
                }
                tit="Total de mascotas"
                total={datas.length}
              />
              <Table
                columns={columns}
                dataSource={datas}
                rowKey={(data) => data.id}
                loading={response.loading}
                scroll={true}
                className="Tabs"
              />

              {dat ? (
                <Modal
                  title={`${dat.name}`}
                  visible={visible}
                  footer={false}
                  onCancel={() => handleCancel()}
                  style={{ width: "70%" }}
                >
                  <div className="content-modal">
                    <Avatar src={IMAGES_PATH + dat.avatar} size={90} />
                    <div
                      style={{
                        display: "flex",
                        textAlign: "center",
                        alignSelf: "center",
                        justifyContent: "center",
                        marginTop: 10,
                      }}
                    >
                      <h2>{dat.name}</h2>
                    </div>
                    <div style={{ textAlign: "left" }}>
                      <Descriptions title="Caracteristicas" layout="vertical">
                        <Descriptions.Item label="Especie">
                          {dat.especie}
                        </Descriptions.Item>
                        <Descriptions.Item label="Raza">
                          {dat.raza}
                        </Descriptions.Item>
                        <Descriptions.Item label="Genero">
                          {dat.genero}
                        </Descriptions.Item>
                        <Descriptions.Item label="Peso">
                          {dat.peso} KG
                        </Descriptions.Item>
                      </Descriptions>
                      <Descriptions title="Detalles" layout="vertical">
                        <Descriptions.Item label="Nacimiento">
                          {moment(dat.age).format("LL")}
                        </Descriptions.Item>
                        <Descriptions.Item label="MicroChip código">
                          {dat.microchip}
                        </Descriptions.Item>
                        <Descriptions.Item label="Vacunas">
                          {dat.vacunas}
                        </Descriptions.Item>
                        <Descriptions.Item label="Alergías">
                          {dat.alergias}
                        </Descriptions.Item>
                      </Descriptions>

                      <div style={{ marginTop: 25, marginBottom: 25 }}>
                        <h3>Historial de la mascota</h3>
                      </div>
                      <Query query={GET_DIAGNOSTICO} variables={{ id: dat.id }}>
                        {({ loading, error, data }) => {
                          console.log(data);
                          if (loading) {
                            return (
                              <div className="page-loader">
                                <Spin size="large"></Spin>
                              </div>
                            );
                          }
                          if (error) {
                            return console.log("error in mascota", error);
                          }
                          if (data) {
                            const datos =
                              data && data.getDiagnostico
                                ? data.getDiagnostico.list
                                : "";

                            return (
                              <List
                                itemLayout="vertical"
                                size="large"
                                pagination={{
                                  onChange: (page) => {
                                    console.log(page);
                                  },
                                  pageSize: 3,
                                }}
                                dataSource={datos}
                                renderItem={(item) => (
                                  <List.Item key={item._id}>
                                    <List.Item.Meta
                                      avatar={
                                        <Avatar
                                          src={
                                            IMAGES_PATH +
                                            item.profesional.avatar
                                          }
                                        />
                                      }
                                      title={
                                        <p>
                                          {item.profesional.nombre}{" "}
                                          {item.profesional.apellidos}
                                        </p>
                                      }
                                      description={
                                        <p>
                                          {moment(item.create_at).format("LL")}
                                        </p>
                                      }
                                    />
                                    {item.diagnostico}
                                  </List.Item>
                                )}
                              />
                            );
                          } else {
                            return (
                              <div className="page-loader">
                                <Spin size="large"></Spin>
                              </div>
                            );
                          }
                        }}
                      </Query>
                    </div>
                  </div>
                </Modal>
              ) : null}
            </div>
          );
        }
      }}
    </Query>
  );
};

export default Mascotas;
