import React, { useState, createElement } from "react";
import {
  Table,
  Tag,
  Input,
  Spin,
  Button,
  Modal,
  message,
  Avatar,
  Descriptions,
  Pagination,
  Comment,
  Rate,
  Tooltip,
  Result,
  Select,
  Switch,
  Form,
} from "antd";
import Header from "../header";
import { Query, Mutation } from "react-apollo";
import { GET_PROFESIONAL_SEARCH, PAGO_QUERY } from "../../../query/index";
import {
  ELIMINAR_PROFESIONAL,
  ACTUALIZAR_PROFESIONAL,
} from "../../../mutations/index";
import {
  ExclamationCircleOutlined,
  CheckCircleFilled,
  StarOutlined,
  DislikeFilled,
  DislikeOutlined,
  LikeFilled,
  LikeOutlined,
  StarTwoTone,
} from "@ant-design/icons";
import "../index.css";
import { IMAGES_PATH } from "../../../constant/consfig";
import moment from "moment";
import "moment/locale/es";

const { Option } = Select;

const { Search } = Input;
const Profesionales = () => {
  const [search, setSearch] = useState("");
  const [visible, setVisible] = useState(false);
  const [dat, setDat] = useState(null);
  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [action, setAction] = useState(null);
  const [consultaID, setConsultaID] = useState(null);
  const [isVerified, setIsVerified] = useState(dat ? dat.isVerified : "");

  let refetch;

  function onChange(value) {
    console.log(`selected ${value}`);
    setConsultaID(value);
  }

  function onBlur() {
    console.log("blur");
  }

  function onFocus() {
    console.log("focus");
  }

  function onSearch(val) {
    console.log("search:", val);
  }

  const showModal = (dataIndex) => {
    setDat(dataIndex);
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
    setDat(null);
  };

  const like = () => {
    setLikes(1);
    setDislikes(0);
    setAction("liked");
  };

  const dislike = () => {
    setLikes(0);
    setDislikes(1);
    setAction("disliked");
  };

  const onSwitchChangeava = () => {
    if (isVerified === true) {
      setIsVerified(false);
    } else {
      setIsVerified(true);
    }
  };

  const actions = [
    <span key="comment-basic-like">
      <Tooltip title="Like">
        {createElement(action === "liked" ? LikeFilled : LikeOutlined, {
          onClick: like,
        })}
      </Tooltip>
      <span className="comment-action">{likes}</span>
    </span>,
    <span key=' key="comment-basic-dislike"'>
      <Tooltip title="Dislike">
        {React.createElement(
          action === "disliked" ? DislikeFilled : DislikeOutlined,
          {
            onClick: dislike,
          }
        )}
      </Tooltip>
      <span className="comment-action">{dislikes}</span>
    </span>,
    <span key="comment-basic-reply-to">Utíl</span>,
  ];

  const handleEliminar = (eliminarProfesional, id) => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que desea eliminar este profesional?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Al eliminar este profesional perderemos toda la información relacionada a el",
      onOk() {
        return new Promise((resolve, reject) => {
          eliminarProfesional({ variables: { id } }).then(async ({ data }) => {
            if (
              data &&
              data.eliminarProfesional &&
              data.eliminarProfesional.success
            ) {
              message.success(data.eliminarProfesional.message);
              resolve();
              refetch();
            } else if (
              data &&
              data.eliminarProfesional &&
              !data.eliminarProfesional.success
            )
              setTimeout(() => {
                message.error(data.eliminarProfesional.message);
              }, 500);
            reject();
          });
        }).catch(() => console.log("error confirming eliminar usuario!"));
      },
      onCancel() {},
    });
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
      render: (text) => <p>{text}</p>,
    },
    {
      title: "Nombre",
      dataIndex: "nombre",
      key: "nombre",
      render: (text) => <p>{text} </p>,
    },
    {
      title: "Apellidos",
      dataIndex: "apellidos",
      key: "apellidos",
      render: (text) => <p>{text}</p>,
    },

    {
      title: "Ciudad",
      dataIndex: "ciudad",
      key: "ciudad",
      render: (text) => <p>{text}</p>,
    },
    {
      title: "Profesión",
      key: "profesion",
      dataIndex: "profesion",
      render: (tags) => <Tag color="geekblue">{tags}</Tag>,
    },
    {
      title: "Detalles",
      key: "x",
      render: (dataIndex) => {
        return (
          <span className="btnflex">
            <Button
              type="primary"
              shape="round"
              className="btnflex"
              style={{ marginRight: 25 }}
              onClick={() => showModal(dataIndex)}
            >
              Ver Detalles
            </Button>
            <Mutation mutation={ELIMINAR_PROFESIONAL}>
              {(eliminarProfesional) => {
                return (
                  <Button
                    type="danger"
                    shape="round"
                    className="btnflex"
                    onClick={() =>
                      handleEliminar(eliminarProfesional, dataIndex.id)
                    }
                  >
                    Eliminar
                  </Button>
                );
              }}
            </Mutation>
          </span>
        );
      },
    },
  ];

  return (
    <Query query={GET_PROFESIONAL_SEARCH} variables={{ search: search }}>
      {(response) => {
        refetch = response.refetch;
        if (response.error) {
          return (
            <div className="page-loader">
              <Spin size="large"></Spin>
            </div>
          );
        }
        if (response) {
          const datas =
            response && response.data && response.data.getProfesionalSearchadmin
              ? response.data.getProfesionalSearchadmin.data
              : "";

          const datss = dat ? dat.professionalRatinglist : "";
          let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
          datss &&
            datss.forEach((start) => {
              if (start.rate === 1) rating["1"] += 1;
              else if (start.rate === 2) rating["2"] += 1;
              else if (start.rate === 3) rating["3"] += 1;
              else if (start.rate === 4) rating["4"] += 1;
              else if (start.rate === 5) rating["5"] += 1;
            });

          const ar =
            (5 * rating["5"] +
              4 * rating["4"] +
              3 * rating["3"] +
              2 * rating["2"] +
              1 * rating["1"]) /
            datss.length;
          let averageRating = 0;
          if (datss.length) {
            averageRating = ar.toFixed(1);
          }

          return (
            <div>
              <Header
                title="Profesionales"
                extra={
                  <Search
                    placeholder="Buscar profesional"
                    style={{ width: 500 }}
                    enterButton="Buscar"
                    size="large"
                    onSearch={(value) => setSearch(value)}
                  />
                }
                tit="Total de profeisonales"
                total={datas.length}
              />
              <Table
                columns={columns}
                dataSource={datas}
                rowKey={(data) => data.id}
                loading={response.loading}
                scroll={true}
                className="Tabs"
              />

              {dat ? (
                <Modal
                  title={`${dat.nombre} ${dat.apellidos}`}
                  visible={visible}
                  footer={false}
                  onCancel={() => handleCancel()}
                  style={{ width: "70%" }}
                >
                  <div className="content-modal">
                    <Avatar src={IMAGES_PATH + dat.avatar} size={90} />
                    <div
                      style={{
                        display: "flex",
                        textAlign: "center",
                        alignSelf: "center",
                        justifyContent: "center",
                        marginTop: 10,
                      }}
                    >
                      <h2>
                        {dat.nombre} {dat.apellidos}
                      </h2>
                      {dat.isVerified ? (
                        <CheckCircleFilled
                          style={{
                            marginLeft: 10,
                            marginTop: 8,
                            fontSize: 18,
                            color: "#647dee;",
                          }}
                        />
                      ) : null}
                    </div>
                    <section className="ddd">
                      <Mutation mutation={ACTUALIZAR_PROFESIONAL}>
                        {(actualizarProfesional) => (
                          <Form
                            layout="horizontal"
                            size="large"
                            onFinish={(values) => {
                              const input = {
                                id: dat.id,
                                isVerified: isVerified,
                              };
                              console.log("input; ", input);
                              actualizarProfesional({ variables: { input } })
                                .then((res) => {
                                  console.log("input; ", res);
                                  message.success(
                                    "El profesional ha sido cambiado a verificado"
                                  );
                                })
                                .catch((err) =>
                                  message.error(
                                    "Algo salió mal. Por favor, vuelva a intentarlo"
                                  )
                                );
                            }}
                          >
                            <Form.Item label="Marcar profesional como veríficado">
                              <Switch
                                defaultChecked={dat ? dat.isVerified : ""}
                                onChange={() => onSwitchChangeava()}
                              />
                            </Form.Item>
                            <Form.Item>
                              <Button
                                shape="round"
                                htmlType="submit"
                                type="primary"
                              >
                                Guardar cambios
                              </Button>
                            </Form.Item>
                          </Form>
                        )}
                      </Mutation>
                    </section>
                    <div style={{ textAlign: "left" }}>
                      <Descriptions title="Info" layout="vertical">
                        <Descriptions.Item label="Precio por consulta">
                          {dat.Precio}€
                        </Descriptions.Item>
                        <Descriptions.Item label="Telefono">
                          {dat.telefono}
                        </Descriptions.Item>
                        <Descriptions.Item label="Email">
                          {dat.email}
                        </Descriptions.Item>
                        <Descriptions.Item label="Descripción" span={4}>
                          {dat.descricion}
                        </Descriptions.Item>
                        <Descriptions.Item label="Profesional desde">
                          {moment(Number(dat.created_at)).format("LL")}
                        </Descriptions.Item>
                        <Descriptions.Item label="Experiencia">
                          {dat.experiencia}
                        </Descriptions.Item>
                        <Descriptions.Item label="Consultas">
                          ({dat.consultas.length}) Consultas realizadas
                        </Descriptions.Item>
                      </Descriptions>
                      <section>
                        <h2>Datos de bancarío para pagos</h2>
                        <Query query={PAGO_QUERY} variables={{ id: dat.id }}>
                          {({ loading, data }) => {
                            if (loading) {
                              return (
                                <div className="page-loader">
                                  <Spin size="large"></Spin>
                                </div>
                              );
                            } else {
                              let response = { data: data };
                              const datos =
                                response &&
                                response.data &&
                                response.data.getPago
                                  ? response.data.getPago.data
                                  : "";
                              return (
                                <div className="Containers-components">
                                  <h3>Nombre: {datos ? datos.nombre : ""}</h3>
                                  <p>IBAN: {datos ? datos.iban : ""}</p>
                                </div>
                              );
                            }
                          }}
                        </Query>
                      </section>
                      <section>
                        <div>
                          <Select
                            showSearch
                            style={{ width: "100%", marginBottom: 50 }}
                            placeholder="Buscar consultas"
                            optionFilterProp="children"
                            onChange={onChange}
                            onFocus={onFocus}
                            onBlur={onBlur}
                            onSearch={onSearch}
                            filterOption={(input, option) =>
                              option.children
                                .toLowerCase()
                                .indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {dat &&
                              dat.consultas.map((mas, index) => {
                                return (
                                  <Option key={index} value={mas.id}>{`${
                                    mas && mas.usuario ? mas.usuario.nombre : ""
                                  } ${
                                    mas && mas.usuario
                                      ? mas.usuario.apellidos
                                      : ""
                                  }  De fecha  ${moment(
                                    Number(mas.created_at)
                                  ).format("LL")}`}</Option>
                                );
                              })}
                          </Select>
                          {consultaID ? (
                            <div>
                              <h3>ID de la consulta seleccionada</h3>
                              <p>{consultaID}</p>
                            </div>
                          ) : null}
                        </div>
                      </section>
                    </div>
                    <div className="valorecion">
                      <div className="tot">
                        <h1>{averageRating}</h1>
                      </div>
                      <div>
                        <div style={{ display: "flex", marginTop: 15 }}>
                          <StarOutlined
                            style={{ color: "#FFA500", fontSize: 24 }}
                          />
                          <StarOutlined
                            style={{ color: "#FFA500", fontSize: 24 }}
                          />
                          <StarOutlined
                            style={{ color: "#FFA500", fontSize: 24 }}
                          />
                          <StarOutlined
                            style={{ color: "#FFA500", fontSize: 24 }}
                          />
                          <StarOutlined
                            style={{ color: "#FFA500", fontSize: 24 }}
                          />
                        </div>
                        <p style={{ marginTop: 5 }}>
                          ({dat && dat.professionalRatinglist.length}) Opiniones
                          de clientes
                        </p>
                      </div>
                    </div>

                    {dat && dat.professionalRatinglist.length > 0 ? (
                      <div>
                        {dat &&
                          dat.professionalRatinglist.map((rat, index) => (
                            <div key={index} style={{ textAlign: "left" }}>
                              <Comment
                                actions={actions}
                                author={
                                  <a>
                                    {rat.customer.nombre}{" "}
                                    {rat.customer.apellidos}
                                  </a>
                                }
                                key={index}
                                style={{ marginTop: 40 }}
                                avatar={
                                  <Avatar
                                    src={IMAGES_PATH + rat.customer.avatar}
                                    alt={rat.customer.nombre}
                                  />
                                }
                                content={<span>{rat.coment}</span>}
                                datetime={
                                  <Tooltip
                                    title={moment(
                                      Number(rat.updated_at)
                                    ).fromNow()}
                                  >
                                    <span>
                                      <Rate disabled defaultValue={rat.rate} />{" "}
                                      ({rat.rate})
                                    </span>
                                  </Tooltip>
                                }
                              />
                            </div>
                          ))}

                        <div
                          style={{
                            marginTop: 30,
                            marginBottom: 30,
                            textAlign: "center",
                          }}
                        >
                          <Pagination
                            defaultCurrent={5}
                            total={dat && dat.professionalRatinglist.length}
                          />
                        </div>
                      </div>
                    ) : null}

                    {dat &&
                    ((dat && dat.professionalRatinglist.length === 0) ||
                      !dat) ? (
                      <div className="no-products">
                        <Result
                          icon={<StarTwoTone style={{ color: "#FFA500" }} />}
                          title="Este profesional aún no tiene valoraciones"
                        />
                      </div>
                    ) : null}
                  </div>
                </Modal>
              ) : null}
            </div>
          );
        }
      }}
    </Query>
  );
};

export default Profesionales;
