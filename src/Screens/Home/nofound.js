import React, { Component } from "react";
import "antd/dist/antd.css";
import { Result, Button } from "antd";

class Notfound extends Component {
  render() {
    return (
      <div style={{marginTop: 0}}>
        <Result
          status="404"
          title="404"
          subTitle="Lo sentimos, la página que visitaste no existe."
          extra={<Button href='/' type="primary">Volver al inico</Button>}
        />
       
      </div>
    );
  }
}

export default Notfound;
