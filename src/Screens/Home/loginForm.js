import React, { useState } from "react";
import { Form, Input, Button, Checkbox, message } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import "./index.css";
import { AUTENTICAR_ADMIN } from "../../mutations";
import { Mutation } from "react-apollo";

const LoginForm = (props) => {
  const [valores, setValores] = useState({
    email: "",
    password: "",
  });

  const history = useHistory();

  const onCompletedLogin = async (res) => {
    console.log("res: ", res);
    if (!res.autenticaradmin.success) {
      message.error(res.autenticaradmin.message);
    } else {
      await localStorage.setItem("token", res.autenticaradmin.data.token);
      await localStorage.setItem("id", res.autenticaradmin.data.id);
      message.success(res.autenticaradmin.message);
      history.push("/welcome");
    }
  };

  return (
    <Mutation
      mutation={AUTENTICAR_ADMIN}
      variables={{ email: valores.email, password: valores.password }}
      onCompleted={onCompletedLogin}
    >
      {(mutation) => {
        return (
          <React.Fragment>
            <Form
              name="normal_login"
              className="login-form"
              initialValues={{
                remember: true,
              }}
              onFinish={(values) => {
                setValores({
                  email: values.email,
                  password: values.password,
                });
                mutation();
              }}
            >
              <Form.Item
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Por favor ingrese su email!",
                  },
                ]}
              >
                <Input
                  prefix={<MailOutlined className="site-form-item-icon" />}
                  placeholder="Email"
                />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Por favor ingrese su contraseña!",
                  },
                ]}
              >
                <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Contraseña"
                />
              </Form.Item>
              <Form.Item>
                <Form.Item name="remember" valuePropName="checked" noStyle>
                  <Checkbox>Recordarme</Checkbox>
                </Form.Item>

                <a
                  className="login-form-forgot"
                  href="/resetPassword"
                  style={{ color: "#647dee;" }}
                >
                  ¿Lo olvidaste?
                </a>
              </Form.Item>

              <Form.Item>
                <Button
                  shape="round"
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                >
                  Iniciar Sesión
                </Button>
              </Form.Item>
            </Form>
          </React.Fragment>
        );
      }}
    </Mutation>
  );
};

export default LoginForm;
