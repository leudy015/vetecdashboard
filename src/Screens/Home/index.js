import React from "react";
import LoginForm from "./loginForm";
import "./index.css";

function Home() {
  return (
    <>
      <div className="containers">
        <div className="Containerform">
          <p className="psol">Bienvenido a Vetec for Team</p>
          <LoginForm />
        </div>
      </div>
    </>
  );
}

export default Home;
