import React from 'react';
import {Menu, Dropdown, Button, Row } from 'antd';
import { GlobalOutlined } from '@ant-design/icons';
import './index.css'

export const menu = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="/welcome">
      🇪🇸 Español
      </a>
    </Menu.Item>
    <Menu.Item>
      <a rel="noopener noreferrer" href="/welcome">
      🇬🇧 Ingles
      </a>
    </Menu.Item>
    <Menu.Item>
      <a rel="noopener noreferrer" href="/welcome">
      🇵🇹 Protugues
      </a>
    </Menu.Item>
  </Menu>
);

export const DropdownMenu = () => {
  return (
    <Dropdown key="more" overlay={menu}>
      <Button
        style={{
          border: 'none',
          padding: 0,
        }}
      >
        <GlobalOutlined 
          style={{
            fontSize: 28,
            marginLeft: 15
          }}
        />
      </Button>
    </Dropdown>
  );
};

export const Content = ({ children, extraContent }) => {
  return (
    <Row>
      <div style={{ flex: 1 }}>{children}</div>
      <div className="image">{extraContent}</div>
    </Row>
  );
};

