import React from 'react'
import { Form, Input, Button, Checkbox, message } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import './Forgot.css'
import { LOCAL_API_URL } from '../../constant/consfig';
import { useHistory } from "react-router-dom";

const NormalforgotForm = () => {

const history = useHistory()

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={ value => {
        const handleSubmit = () => {
            const emails = value.email
             if (emails) {
              fetch(`${LOCAL_API_URL}/forgotpassword-team?email=${emails}`)
                .then(res => {
                  console.log(res)
                  if (res) {
                    message.success('Mensaje enviado ve a tu bandeja de entrada')
                    history.push('/');
                  } else {
                    message.error('hay un problema con tu solicitud')
                  }
                }).catch(err => console.log('err:', err))
            }
          };
          
          handleSubmit()
      }}
    >
      <Form.Item
        name="email"
        rules={[{ required: true, message: 'Por favor ingresa tu email!' }]}
      >
        <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
      </Form.Item>
    
      <Form.Item>
        <Button shape="round" type="primary" htmlType="submit" className="login-form-button">
          Enviar enlace
        </Button>
      </Form.Item>
    </Form>
  );
};

export default NormalforgotForm;