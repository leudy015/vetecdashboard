import React, {useEffect, useState} from 'react'
import './Forgot.css'
import ReserForm from './ResetForm';
import { LOCAL_API_URL } from '../../constant/consfig';
import axios from 'axios';



const ResetTeam = props =>{

    const [ resut, setResult ] = useState({
        email: '',
        isValid: false,
        set: false
    })

    useEffect(()=>{
        // console.log(this.props.match.params.token);
        const url = LOCAL_API_URL + '/tokenValidation-team';
        axios
          .post(url, props.match.params)
          .then(res => {
            console.log(res.data);
            setResult({
              isValid: res.data.isValid,
              email: res.data.email,
              set: true
            });
          })
          .catch(err => {
            console.log('err:', err);
          });
    }, []) 
        

    return(
        <div>
        <div className="containers">
        <div style={{paddingTop: 150}}>
            <div className="Containerform">
            <p className="psol">
             Escribe tu nueva contraseña!
            </p>
            {resut.isValid && (
              <ReserForm email={resut.email} token={props.match.params.token}/>
            )}
            {!resut.isValid && resut.set && (
              <p>Token no valido intentalo de nuevo.</p>
            )}
            </div>
        </div>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#f3f4f5" fill-opacity="1" d="M0,224L40,186.7C80,149,160,75,240,37.3C320,0,400,0,480,37.3C560,75,640,149,720,181.3C800,213,880,203,960,181.3C1040,160,1120,128,1200,122.7C1280,117,1360,139,1400,149.3L1440,160L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path></svg>
        </div>
    )
}

export default ResetTeam;