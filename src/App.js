import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import PrivateRoute from './PrivateRouter';
import PublicRoute from './PublicRouter';

import * as Sentry from '@sentry/browser';
import Session from './Session';

///////////// Screens /////////////////
import Home from './Screens/Home/index';
import Welcome from './Screens/Welcome';
import Notfound from './Screens/Home/nofound';
import ForgotTeam from './Screens/ForgotPassword';
import ResetTeam from './Screens/ForgotPassword/Reset';



/////////////// End //////////////////


Sentry.init({
  dsn: 'https://f955be9c777e4aecb34bb34a01e90f36@sentry.io/1778988'
});

class App extends Component {
  render(){
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <PublicRoute exact path="/" component={Home} />
            <PrivateRoute exact path="/welcome" component={Welcome} />
            <PublicRoute  refetch={this.props.refetch} exact path="/resetPassword-team/:token" component={ResetTeam} />
            <PublicRoute  refetch={this.props.refetch} exact path="/resetPassword" component={ForgotTeam} />
            <Route  component={Notfound} />
            </Switch>
        </BrowserRouter>
        </div>
    );

  }
}

const RootSession = Session(App)

export { RootSession };

