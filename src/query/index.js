import gql from 'graphql-tag';

export const ADMIN_ACTUAL = gql`
  query obtenerAdmin {
    obtenerAdmin {
      id
      email
      nombre
      apellidos
    }
  }
`;


export const ADMIN_DETAIL = gql`
query getAdmin($id: ID!) {
  getAdmin(id: $id) {
    id
    email
    UserID
    nombre
    apellidos
    ciudad
    telefono
    avatar
    cargo
    descricion
    twitter
    linkedin
    notificacion
    createBy
  }
}
`;


export const PAGO_QUERY = gql`
  query getPago($id: ID!) {
      getPago(id: $id) {
        success
        message
        data {
          id
          iban
          nombre
          profesionalID
          }
        }
      }
    `;

export const GET_TEAM = gql`
query getTeam($id: ID!) {
  getTeam(id: $id) {
    success
    message
    data{
    id
    email
    UserID
    nombre
    apellidos
    ciudad
    telefono
    avatar
    cargo
    descricion
    twitter
    linkedin
    notificacion
    createBy
  }
  }
}
`;

export const GET_TEAM_SEARCH = gql`
{
  getTeamSearch {
    success
    message
    data{
      id
      email
      UserID
      nombre
      apellidos
      ciudad
      telefono
      avatar
      cargo
      descricion
      twitter
      linkedin
      notificacion
      createBy
    }
  }
}
`;


export const GET_ALL_CUPON = gql`
{
  getAllCupon {
    id
    clave
    descuento
    tipo
  }
}
`;

export const CONSULTA_BY_USUARIO = gql`
  query getConsultaByUsuario($usuarios: ID!, $dateRange: DateRangeInput) {
    getConsultaByUsuario(usuarios: $usuarios, dateRange: $dateRange) {
      success
      message
      list{
      id
      time
      estado
      nota
      endDate
      cantidad
      created_at
      mascota{
        id
        name
        age
        usuario
        avatar
        especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
      }
      usuario{
        id
        email
        UserID
        nombre
        apellidos
        ciudad
        telefono
        avatar
        notificacion
      }
      profesional{
        id
        email
        nombre
        apellidos
        UserID
        ciudad
        telefono
        avatar
        profesion
        experiencia
        descricion
        Precio
        notificacion
        isProfesional
        isVerified
        isAvailable
        professionalRatinglist{
      id
      customer{
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        avatar
        UserID
      }
      rate
      updated_at
      coment
    }
      }
    }
    }
  }
`;

export const GET_NOTIFICATIONS = gql`
query getNotifications($Id: ID!) {
  getNotifications(Id: $Id) {
      success
      message
      notifications{
        _id
        type
        read
        createdAt
        user
        profesional{
          id
          nombre
          apellidos
         	avatar
        }
        usuario{
          id
          nombre
          apellidos
          avatar
        }
      }
  }
}
`;

export const CONSULTA_BY_ADMIN = gql`
  query getConsultaadmin($search: String $dateRange: DateRangeInput) {
    getConsultaadmin(search: $search dateRange: $dateRange) {
      success
      message
      list{
      id
      time
      estado
      nota
      endDate
      cantidad
      created_at
      usuario{
        id
        email
        UserID
        nombre
        apellidos
        ciudad
        telefono
        avatar
        notificacion
      }
      mascota{
        id
        name
        age
        usuario
        avatar
        especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
      }
      profesional{
        id
        email
        nombre
        apellidos
        UserID
        ciudad
        telefono
        avatar
        profesion
        experiencia
        descricion
        Precio
        notificacion
        isProfesional
        isVerified
        isAvailable
        professionalRatinglist{
      id
      customer{
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        avatar
        UserID
      }
      rate
      updated_at
      coment
    }
      }
    }
    }
  }
`;

export const GET_MASCOTAS = gql`
 query getMascota($usuarios: ID!) {
    getMascota(usuarios: $usuarios){
    message
    success
    list{
      id
      name
      age
      usuario
      avatar
      especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
      }
    } 
  }
`;

export const GET_MASCOTAS_ADMIN = gql`
 query getMascotaadmin($id: ID!) {
  getMascotaadmin(id: $id){
    message
    success
    list{
      id
      name
      age
      usuario
      avatar
      especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
      }
    } 
  }
`;

export const GET_DIAGNOSTICO = gql`
query getDiagnostico($id: ID!) {
  getDiagnostico(id: $id) {
    message
    success
    list{
      _id
      profesional{
        nombre
        apellidos
        profesion
        avatar
      }
      diagnostico
      mascota
      created_at
    }
  }
}
`;




export const GET_PROFESIONAL_SEARCH = gql`
query  getProfesionalSearchadmin($search: String){
  getProfesionalSearchadmin(search: $search){
    message
    success
    data{
      id
      email
      nombre
      apellidos
      ciudad
      telefono
      UserID
      avatar
      profesion
      experiencia
      descricion
      Precio
      created_at
      notificacion
      isProfesional
      isVerified
      isAvailable
      isVerified
      professionalRatinglist{
      id
      customer{
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        avatar
        UserID
      }
      rate
      updated_at
      coment
    }
    consultas{
      id
      created_at
      usuario {
        id
        nombre
        apellidos
      }
    }
      
    }
  }
}
`;


export const GET_CLIENTE_SEARCH = gql`
query  getClientesSearchadmin($search: String){
  getClientesSearchadmin(search: $search){
    message
    success
    data{
      id
      email
      UserID
      nombre
      apellidos
      ciudad
      telefono
      avatar
      notificacion
      created_at
    }
  }
}
`;