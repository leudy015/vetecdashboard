import React from "react";
import { Query } from "react-apollo";
import { ADMIN_ACTUAL } from "./query";

const Session = (Component) => (props) => (
  <Query query={ADMIN_ACTUAL}>
    {({ loading, error, data, refetch }) => {
      if (loading) return null;
      if (error) return console.log(error);
      return <Component {...props} refetch={refetch} session={data} />;
    }}
  </Query>
);

export default Session;
